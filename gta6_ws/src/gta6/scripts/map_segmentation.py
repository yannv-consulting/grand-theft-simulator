#!/usr/bin/env python

import numpy as np
from PIL import Image

class Map:
    
    def __init__(self, map_name):
        self.map_name = map_name

        self.pedestrian = (255, 14, 0)
        self.building = (29, 44, 77)
        self.road1 = (44, 102, 117)
        self.road2 = (48, 74, 125)
        self.road3 = (40, 61, 106)
        self.grass = (2, 62, 88)
        self.water = (14, 22, 38)

        self.building_rgba = (29, 44, 77, 255)
        self.water_rgba = (14, 22, 38, 255)
        self.other_rgba = (0, 0, 0, 0)

        self.width = 1780
        self.height = 1010

        self.pedestrian_number = 1
        self.building_number = 2
        self.road1_number = 3
        self.road2_number = 6
        self.road3_number = 7
        self.grass_number = 4
        self.water_number = 5
        self.void_number = 0

        self.pedestrian_black_white = (0, 0, 0)
        self.building_black_white = (0, 0, 0)
        self.road1_black_white = (255, 255, 255)
        self.road2_black_white = (240, 240, 240)
        self.road3_black_white = (220, 220, 220)
        self.grass_black_white = (150, 150, 150)
        self.water_black_white = (0, 0, 0)
        self.void_black_white = (255, 255, 255)

        self.pedestrian_cost = 0
        self.building_cost = 100
        self.road1_cost = 1
        self.road2_cost = 2
        self.road3_cost = 3
        self.grass_cost = 5
        self.water_cost = 100
        self.void_cost = 1

        self.pixel_coef = 10

    def image_to_crop_and_rgb_image_path(self, image_path):
        path_rgb = self.save_image_rgb(image_path)
        path_crop = self.save_crop_image(path_rgb)
        return path_crop

    def crop_image_to_matrix_rgb(self, path_crop):
        matrix_rgb = self.matrix_image_pixels(path_crop)
        return matrix_rgb

    def matrix_to_cost_map_image_path(self, matrix):
        matrix_black_and_white = self.matrix_black_white(matrix)
        black_and_white_path = self.matrix_to_image(matrix_black_and_white, "src/maps/" + self.map_name + "/map_black_white.png")
        low_res_path = self.save_image_low_resolution(black_and_white_path)
        return low_res_path

    def cost_map_image_path_to_cost_matrix(self, low_res_path):
        cost_matrix = self.matrix_image_cost(low_res_path)
        return cost_matrix

    def image_to_mask_image(self, matrix):
        rgba_path = "src/maps/" + self.map_name + "/map_rgba.png"
        matrix_rgba = self.matrix_segmentation_image_rgba(matrix)
        self.matrix_to_image(matrix_rgba, rgba_path, True)
        return rgba_path

    # Convert to RGB
    # input: map image path
    # output: RGB map image path
    def save_image_rgb(self, image_path):
        image_rgb_path = "src/maps/" + self.map_name + "/map_rgb.png"
        image_rgb = Image.open(image_path, 'r')
        image_rgb.convert("RGB").save(image_rgb_path)
        image_rgb.close()
        return image_rgb_path
 
    # Convert to RGBA
    # input: map image path
    # output: RGBA map image path
    def save_image_rgba(self, image_path):
        image_rgba_path = "src/maps/" + self.map_name + "/map_rgba.png"
        image_rgba = Image.open(image_path, 'r')
        image_rgba.convert("RGBA").save(image_rgba_path)
        image_rgba.close()
        return image_rgba_path
    
    # Crop image to make sure it width and it height are a multiple of self.pixel_coef
    # input: Map image path
    # output: Map image path with width and height mutiples of self.pixel_coef
    def save_crop_image(self, image_path):
        image_crop_path = "src/maps/" + self.map_name + "/mapcrop.png"
        image_crop = Image.open(image_path, 'r')
        width,height = image_crop.size
        left = 0
        top = 0
        width = (width // self.pixel_coef) * self.pixel_coef
        height = (height // self.pixel_coef) * self.pixel_coef
        box = (left, top, left+width, top+height)
        image_crop = image_crop.crop(box)
        image_crop.save(image_crop_path)
        return image_crop_path

    # Return a matrix with the color data of the image
    # input: image path
    # output: matrix of image data (RGB)
    def matrix_image_pixels(self, image_path):
        image = Image.open(image_path, 'r')
        self.width, self.height = image.size
        pixel_values = list(image.getdata())
        pixel_values = [pixel_values[i * self.width:(i + 1) * self.width] for i in range(self.height)]
        return pixel_values

    # Return a matrix with the cost data of the image
    # input: image path
    # output: matrix of cost data [0:255]
    def matrix_image_cost(self, image_path):
        image = Image.open(image_path, 'r')
        self.width, self.height = image.size
        cost_values = list(image.getdata(0))
        cost_values = [cost_values[i * self.width:(i + 1) * self.width] for i in range(self.height)]
        return cost_values
    
    # Create a matrix which contain the number associated with each obstacles (pedestrians, buildings, roads, grass, water or void) 
    # input: matrix of image data (RGB)
    # output: matrix of obstacles numbers (0-5)
    def matrix_segmentation_image(self, matrix):
        matrix_seg = np.zeros((len(matrix), len(matrix[0]), 1), dtype=int)
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                if matrix[i][j] == self.building:
                    matrix_seg[i][j] = self.building_number
                elif matrix[i][j] == self.road1:
                    matrix_seg[i][j] = self.road1_number
                elif matrix[i][j] == self.road2:
                    matrix_seg[i][j] = self.road2_number
                elif matrix[i][j] == self.road3:
                    matrix_seg[i][j] = self.road3_number
                elif matrix[i][j] == self.grass:
                    matrix_seg[i][j] = self.grass_number
                elif matrix[i][j] == self.water:
                    matrix_seg[i][j] = self.water_number
                elif matrix[i][j] == self.pedestrian:
                    matrix_seg[i][j] = self.pedestrian_number
                else:
                    matrix_seg[i][j] = self.void_number
        return matrix_seg

    # Create a matrix which contain only the buildings and water 
    # input: matrix of image data (RGBA)
    # output: matrix of building and water data (RGBA)
    def matrix_segmentation_image_rgba(self, matrix):
        matrix_building_water_rgba = np.zeros((len(matrix), len(matrix[0]), 4), dtype=np.uint8)
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                if matrix[i][j] == self.building_number:
                    matrix_building_water_rgba[i, j] = self.building_rgba
                elif matrix[i][j] == self.water_number:
                    matrix_building_water_rgba[i, j] = self.water_rgba
                else:
                    matrix_building_water_rgba[i][j] = self.other_rgba
        return matrix_building_water_rgba

    # Create a matrix which contain the black and white color associated with each obstacles (pedestrians, buildings, roads, grass, water or void) 
    # input: matrix of image data (RGB) 
    # output: matrix of image data (Black and white)
    def matrix_black_white(self, matrix):
        matrix_black_white = np.zeros((len(matrix), len(matrix[0]), 3), dtype=np.uint8)
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                if matrix[i][j] == self.building_number:
                    matrix_black_white[i][j] = self.building_black_white
                elif matrix[i][j] == self.road1_number:
                    matrix_black_white[i][j] = self.road1_black_white
                elif matrix[i][j] == self.road2_number:
                    matrix_black_white[i][j] = self.road2_black_white
                elif matrix[i][j] == self.road3_number:
                    matrix_black_white[i][j] = self.road3_black_white
                elif matrix[i][j] == self.grass_number:
                    matrix_black_white[i][j] = self.grass_black_white
                elif matrix[i][j] == self.water_number:
                    matrix_black_white[i][j] = self.water_black_white
                elif matrix[i][j] == self.pedestrian_number:
                    matrix_black_white[i][j] = self.pedestrian_black_white
                else:
                    matrix_black_white[i][j] = self.void_black_white
        return matrix_black_white

    #
    # input: 
    # output: 
    def matrix_to_image(self, matrix, path, RGBA = None):
        if RGBA:
            image = Image.fromarray(matrix, 'RGBA')
        else:
            image = Image.fromarray(matrix)
        image.save(path)
        image.close()
        return path

    # Rescale image to a quater of it original size
    # input: Map image path with width and height mutiples of self.pixel_coef
    # output: Map image path with width and height divide by self.pixel_coef
    def save_image_low_resolution(self, image_path):
        image_low_path = "src/maps/" + self.map_name + "/map_low.png"
        image = Image.open(image_path, 'r')
        width, height = image.size
        width_low = width // self.pixel_coef
        height_low = height // self.pixel_coef
        size = width_low, height_low
        image.thumbnail(size, Image.ANTIALIAS)
        image.save(image_low_path)
        image.show()
        image.close()
        return image_low_path
