#!/usr/bin/env python

import os
import pygame
from math import sin, radians, degrees, copysign, sqrt, atan
from pygame.math import Vector2
import numpy as np
import pygame
from PIL import Image
import rospy
from geometry_msgs.msg import Pose2D
import math
import canbus

class Car:
    def __init__(self, x, y, angle = 0.0, length = 0.8, max_steering = 80, max_acceleration = 4.0):
        self.position = Vector2(x, y)
        self.velocity = Vector2(0.0, 0.0)
        self.angle = angle
        self.length = length
        self.max_acceleration = max_acceleration
        self.max_steering = max_steering
        self.max_velocity = 1
        self.brake_deceleration = 10
        self.free_deceleration = 2

        self.acceleration = 0.0
        self.steering = 0.0
        
        self.blinkleft_increment = 0
        self.blinkright_increment = 0
        
        self.pub = rospy.Publisher('car_position', Pose2D, queue_size=10)
        rospy.init_node('pos_publisher', anonymous=True)

        # Can Bus
        self.arbitration_id_velocity = 1
        self.arbitration_id_acceleration = 2
        self.arbitration_id_steering = 3

    def update(self, dt):
        self.velocity += (self.acceleration * dt, 0)
        self.velocity.x = max(- self.max_velocity, min(self.velocity.x, self.max_velocity))

        if self.steering:
            turning_radius = self.length / sin(radians(self.steering))
            angular_velocity = self.velocity.x / turning_radius
        else:
            angular_velocity = 0

        self.position += self.velocity.rotate(- self.angle) * dt
        self.angle += degrees(angular_velocity) * dt
        
    def check_collision(self, car_coords, matrice_seg):
        if car_coords[1] < len(matrice_seg) and car_coords[0] < len(matrice_seg[0]) and car_coords[1] > 0 and car_coords[0] > 0:
            if matrice_seg[car_coords[1], car_coords[0]] == 2 or matrice_seg[car_coords[1], car_coords[0]] == 5:
                return True
            else:
                return False
        else:
            return True
    
    def set_max_acceleration(self, car_coords, matrice_seg):
        if car_coords[1] < len(matrice_seg) and car_coords[0] < len(matrice_seg[0]) and car_coords[1] > 0 and car_coords[0] > 0:
            if matrice_seg[car_coords[1], car_coords[0]] == 3: 
                if self.max_velocity > 8:
                    self.max_velocity -= 1
                elif self.max_velocity < 8:
                    self.max_velocity += 1
            elif matrice_seg[car_coords[1], car_coords[0]] == 4: 
                if self.max_velocity > 1:
                    self.max_velocity -= 1
                elif self.max_velocity < 1:
                    self.max_velocity += 1
            elif matrice_seg[car_coords[1], car_coords[0]] == 6: 
                if self.max_velocity > 4:
                    self.max_velocity -= 1
                elif self.max_velocity < 4:
                    self.max_velocity += 1
            elif matrice_seg[car_coords[1], car_coords[0]] == 7: 
                if self.max_velocity > 3:
                    self.max_velocity -= 1
                elif self.max_velocity < 3:
                    self.max_velocity += 1
            else:
                self.max_velocity = 4
        else:
            self.max_velocity = 4

    def position_publisher(self):
        pos = Pose2D()
        pos.x = self.position[0]
        pos.y = self.position[1]
        pos.theta = self.angle
        self.pub.publish(pos)
        
    def auto_drive(self, laser, dt):
        front = False
        left_front = False
        right_front = False
        back = False
        left_back = False
        right_back = False
        front_distance = 1000
        left_front_distance = 1000
        right_front_distance = 1000
        back_distance = 1000
        left_back_distance = 1000
        right_back_distance = 1000
        
        go_backward = False
        backward_counter = 0
        first_angle = True
        path_available_angle = []
        fatal_distance = 40

        for detect in laser:
            if ((detect[2] >= 0 and detect[2] <= 7) or (detect[2] >= 352 and detect [2] <= 359)):
                if detect[3] < front_distance:
                    front_distance = detect[3]
                front = True
            if (detect[2] >= 7 and detect[2] <= 38):
                if detect[3] < left_front_distance:
                    left_front_distance = detect[3]
                left_front = True
            if (detect[2] >= 90 and detect[2] <= 172):
                if detect[3] < left_back_distance:
                    left_back_distance = detect[3]
                left_back = True
            if (detect[2] >= 173 and detect[2] <= 186):
                if detect[3] < back_distance:
                    back_distance = detect[3]
                back = True
            if (detect[2] >= 187 and detect[2] <= 269):
                if detect[3] < right_back_distance:
                    right_back_distance = detect[3]
                right_back = True
            if (detect[2] >= 320 and detect[2] <= 352):
                if detect[3] < right_front_distance:
                    right_front_distance = detect[3]
                right_front = True
            if detect[3] < fatal_distance:
                if not first_angle:
                    if detect[2] - previous_angle > fatal_distance:
                        path_available_angle.append((int((detect[2] + previous_angle)/2), detect[2], previous_angle))
                else:
                    first_angle = False
                previous_angle = detect[2]

        coef_dt = 15
        
        if not go_backward:
            if not front:
                self.speed_up(dt*coef_dt)
            elif (front and front_distance > 40):
                if not go_backward:
                    if not left_front:
                        self.speed_up(dt*coef_dt)
                        self.turn_left(dt*coef_dt)
                    elif not right_front or (right_front and right_front_distance > fatal_distance):
                        self.speed_up(dt*coef_dt)
                        self.turn_right(dt*coef_dt)
                    elif not left_front or (left_front and left_front_distance > fatal_distance):
                        self.speed_up(dt*coef_dt)
                        self.turn_left(dt*coef_dt)
                    else:
                        go_backward = True
            else:
                go_backward = True
        else:
            if not back:
                self.speed_down(dt*coef_dt)
            elif (back and back_distance > fatal_distance):
                    if not left_back:
                        self.speed_down(dt*coef_dt)
                        self.turn_left(dt*coef_dt)
                    elif not right_back or (right_back and right_back_distance > fatal_distance):
                        self.speed_down(dt*coef_dt)
                        self.turn_right(dt*coef_dt)
                    elif not left_back or (left_back and left_back_distance > fatal_distance):
                        self.speed_down(dt*coef_dt)
                        self.turn_left(dt*coef_dt)
                    else:
                        go_backward = False
            else: 
                go_backward = False

        if go_backward:
            backward_counter +=1
        if backward_counter > 20:
            backward_counter = 0
            go_backward = False
        front = False
        left_front = False
        right_front = False
        back = False
        left_back = False
        right_back = False
        
        return path_available_angle

    def police_init_angle(self, path_list):
        if path_list[-2][0] > path_list[-1][0]:
            self.angle = 0
        elif path_list[-2][1] > path_list[-1][1]:
            self.angle = - 90
        elif path_list[-2][0] < path_list[-1][0]:
            self.angle = 180
        else:
            self.angle = 90
            
    def distance(self, coord1, coord2):
        dist = math.sqrt(((coord2[0]-coord1[0])*(coord2[0]-coord1[0]))+((coord2[1]-coord1[1])*(coord2[1]-coord1[1])))
        return dist

    # change the angle of the police car
    def angle_processing(self, car_coords, follow_coords):
        if follow_coords[0] == car_coords[0] and follow_coords[1] > car_coords[1]:
            self.angle = - 90
        elif follow_coords[0] == car_coords[0] and follow_coords[1] < car_coords[1]:
            self.angle = 90
        elif follow_coords[1] == car_coords[1] and follow_coords[0] > car_coords[0]:
            self.angle = 0
        elif follow_coords[1] == car_coords[1] and follow_coords[0] > car_coords[0]:
            self.angle = 180
        elif follow_coords[0] > car_coords[0] and follow_coords[1] > car_coords[1]: # bas droit
            self.angle = 360 - degrees(math.atan(abs(car_coords[1] - follow_coords[1]) / abs(car_coords[0] - follow_coords[0])))
        elif follow_coords[0] < car_coords[0] and follow_coords[1] > car_coords[1]: # bas gauche
            self.angle = 180 + degrees(math.atan(abs(car_coords[1] - follow_coords[1]) / abs(car_coords[0] - follow_coords[0])))
        elif follow_coords[0] > car_coords[0] and follow_coords[1] < car_coords[1]: #haut droit
            self.angle = degrees(math.atan(abs(car_coords[1] - follow_coords[1]) / abs(car_coords[0] - follow_coords[0])))
        else: # haut gauche
            self.angle = 180 - degrees(math.atan(abs(car_coords[1] - follow_coords[1]) / abs(car_coords[0] - follow_coords[0])))
        
    def path_following(self, path_list, car_coords, dt):
        if self.acceleration >= self.max_acceleration:
            self.acceleration = self.max_acceleration
        else:
            self.acceleration += 1 * dt * 16

        pos_to_follow = path_list[-1]
        dist = self.distance(car_coords, pos_to_follow)
        self.angle_processing(car_coords, pos_to_follow)
        if dist < 40:
            pos_to_follow = path_list.pop(-1)
        return path_list

    def command(self, pressed, dt):
        if pressed[pygame.K_UP]:
            self.speed_up(dt)
        elif pressed[pygame.K_DOWN]:
            self.speed_down(dt)
        elif pressed[pygame.K_SPACE]:
            self.brake(dt)
        else:
            self.free(dt)

        if pressed[pygame.K_RIGHT]:
            self.turn_right(dt)
    
        elif pressed[pygame.K_LEFT]:
            self.turn_left(dt)
            
        else:
            self.steering = 0
        self.steering = max(- self.max_steering, min(self.steering, self.max_steering))

    def command_police(self, dt):
        self.free(dt)
        self.steering = 0
        self.steering = max(- self.max_steering, min(self.steering, self.max_steering))

    def brake(self, dt):
        if abs(self.velocity.x) > dt * self.brake_deceleration:
            self.acceleration = - copysign(self.brake_deceleration, self.velocity.x)
        else:
            self.acceleration = - self.velocity.x / dt

    def speed_up(self, dt):
        if self.velocity.x < 0:
            self.acceleration = self.brake_deceleration
        elif self.acceleration >= self.max_acceleration:
            self.acceleration = self.max_acceleration
        else:
            self.acceleration += 1 * dt

    def speed_down(self, dt):
        if self.velocity.x > 0:
            self.acceleration = - self.brake_deceleration
        elif self.acceleration <= - self.max_acceleration:
            self.acceleration = - self.max_acceleration
        else:
            self.acceleration -= 1 * dt

    def free(self, dt):
        if abs(self.velocity.x) > dt * self.free_deceleration:
                self.acceleration = - copysign(self.free_deceleration, self.velocity.x)
        else:
            if dt != 0:
                self.acceleration = -self.velocity.x / dt
                self.acceleration = max(- self.max_acceleration, min(self.acceleration, self.max_acceleration))

    def turn_right(self, dt):
        self.steering -= 30 * dt

    def turn_left(self, dt):
        self.steering += 30 * dt
   
class Lightning:
    def __init__(self):
        self._low_beam = False
        self._high_beam = False
        self._blink_left = False
        self._blink_right = False
        
        self.current_dir = os.path.dirname(os.path.abspath(__file__))
        
        self.low_beam_path = os.path.join(self.current_dir, "../src/vehicule_parts/lowbeam.png")
        self.high_beam_path = os.path.join(self.current_dir, "../src/vehicule_parts/highbeam.png")
        self.blink_left_path = os.path.join(self.current_dir, "../src/vehicule_parts/blinkleft.png")
        self.blink_right_path = os.path.join(self.current_dir, "../src/vehicule_parts/blinkright.png")
        self.car_path = os.path.join(self.current_dir, "../src/vehicules/car.png")
        self.nolights_path = os.path.join(self.current_dir, "../src/vehicule_parts/nolights.png")
        
        self.low_beam_image = pygame.image.load(self.low_beam_path)
        self.high_beam_image = pygame.image.load(self.high_beam_path)
        self.blink_left_image = pygame.image.load(self.blink_left_path)
        self.blink_right_image = pygame.image.load(self.blink_right_path)
        self.car_image = pygame.image.load(self.car_path)
        self.nolights_image = pygame.image.load(self.nolights_path)

        # Can Bus
        self.arbitration_id_light = 4
        self.arbitration_id_blink = 5

        self.data_light_on = 1
        self.data_big_light_on = 2
        self.data_light_off = 0

        self.data_blink_left = 1
        self.data_blink_right = 2
        self.data_blink_off = 0

    def lightDisplay(self, light_state, big_light_state):
        if light_state:
            return self.low_beam_image, self.arbitration_id_light, self.data_light_on
        elif big_light_state:
            return self.high_beam_image, self.arbitration_id_light, self.data_big_light_on
        else:
            return self.nolights_image, self.arbitration_id_light, self.data_light_off

    def blinkDisplay(self, blink_right_state, blink_left_state, blink_increment):
        if blink_left_state and blink_increment <= 5:       
            return self.blink_left_image, self.arbitration_id_blink, self.data_blink_left
        elif blink_left_state and blink_increment > 5 and blink_increment <= 10:
            return self.nolights_image, self.arbitration_id_blink, self.data_blink_left
        elif blink_right_state and blink_increment <= 5:
            return self.blink_right_image, self.arbitration_id_blink, self.data_blink_right
        elif blink_right_state and blink_increment > 5 and blink_increment <= 10:
            return self.nolights_image, self.arbitration_id_blink, self.data_blink_right
        else:
            return self.nolights_image, self.arbitration_id_blink, self.data_blink_off

class Ranger:
    def __init__(self):
        self.front = 0
        self.back = 0
        self.right = 0
        self.left = 0
        
        # Radius parameters
        self.limit = 100
        self.nb_angle = 360
        self.step_distance = 4
        self.step_angle = 3

    def update(self, car_center_coords, car_angle, matrice_seg, map_width, map_height):
        detect = []
        for angle in range(0, self.nb_angle, self.step_angle):
            for dist in range(0, self.limit, self.step_distance):
                x_target = dist * math.cos(car_angle + (angle * math.pi / 180)) + car_center_coords[0]
                y_target = dist * math.sin(- car_angle - (angle * math.pi / 180)) + car_center_coords[1]
                if x_target < len(matrice_seg[0]) and y_target < len(matrice_seg) and x_target > 0 and y_target > 0:
                    if matrice_seg[int(y_target)][int(x_target)] == 2 or matrice_seg[int(y_target)][int(x_target)] == 5:
                        detect.append((int(x_target), int(y_target), angle, dist))
                        break
                else:
                    detect.append((int(x_target), int(y_target), angle, dist))
                    break
        return detect
