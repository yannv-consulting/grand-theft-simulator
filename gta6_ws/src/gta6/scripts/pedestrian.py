#!/usr/bin/env python

import os
import pygame
from math import sin, radians, degrees, copysign
from pygame.math import Vector2
import numpy as np
import os
import pygame
from PIL import Image
import rospy
from geometry_msgs.msg import Pose2D
import map_segmentation

class Pedestrian:
    def __init__(self, x, y, width, height):
        self.zoom_coef = 2
        self.scaling_factor = 32

        # Initialisation of the pedestrian's image
        self.current_dir = os.path.dirname(os.path.abspath(__file__))
        self.pedestrian_path = os.path.join(self.current_dir, "../src/pedestrian/man.png")
        self.pedestrian_image = pygame.image.load(self.pedestrian_path)
        self.pedestrian_image = pygame.transform.scale(self.pedestrian_image, (self.zoom_coef * int(self.scaling_factor), self.zoom_coef * int(self.scaling_factor / 2)))
        self.pedestrian_init_image = pygame.transform.rotate(self.pedestrian_image, 180)

        # Initialisation of the pedestrian's coordonates
        self.init_pedestrian_coords = [x, y]
        self.pedestrian_coords = [x, y]
        self.screen = pygame.display.set_mode((width, height))
        self.initial_direction = True

        self.pub = rospy.Publisher('pedestrian_position', Pose2D, queue_size=10)
        rospy.init_node('pos_publisher', anonymous=True)

    def position_publisher(self):
        # Publish the position of the pedestrian in the ROS topic /pos_publisher
        pos = Pose2D()
        pos.x = self.pedestrian_coords[0]
        pos.y = self.pedestrian_coords[1]
        pos.theta = 0
        self.pub.publish(pos)
        
    def movement(self, path):
        # Initialisation of the waypoints
        waypoint1 = [600, 228]
        waypoint2 = [600, 588]
        waypoint3 = [150, 588]
        waypoint4 = [150, 228]
        
        # Once the pedestrian has arrived to waypoint1, goes to waypoint2
        if path[0] == 1:
            self.pedestrian_image = self.pedestrian_init_image
            self.pedestrian_coords[1] += 2
            if self.pedestrian_coords[1] >= waypoint2[1]:
                 path.remove(1)
                 path.append(1)
        # Once the pedestrian has arrived to waypoint2, goes to waypoint3
        elif path[0] == 2:
            self.pedestrian_image = pygame.transform.rotate(self.pedestrian_init_image, 270)
            self.pedestrian_coords[0] -= 2
            if self.pedestrian_coords[0] <= waypoint3[0]:
                 path.remove(2)
                 path.append(2)
         # Once the pedestrian has arrived to waypoint3, goes to waypoint4
        elif path[0] == 3:
            self.pedestrian_image = pygame.transform.rotate(self.pedestrian_init_image, 180)
            self.pedestrian_coords[1] -= 2
            if self.pedestrian_coords[1] <= waypoint4[1]:
                 path.remove(3)
                 path.append(3)
         # Once the pedestrian has arrived to waypoint4, goes to waypoint1
        elif path[0] == 4:
            self.pedestrian_image = pygame.transform.rotate(self.pedestrian_init_image, 90)   
            self.pedestrian_coords[0] += 2
            if self.pedestrian_coords[0] >= waypoint1[0]:
                 path.remove(4)
                 path.append(4)
