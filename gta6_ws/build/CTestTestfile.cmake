# CMake generated Testfile for 
# Source directory: /fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src
# Build directory: /fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(global_planner_short_path_student/navigation_stage_student_tp)
SUBDIRS(gta6)
SUBDIRS(local_planner_student)
