# Project installation
python3 -m pip install -U setuptools --user\
python3 -m pip install -U pillow --user\
python3 -m pip install -U shapely --user\
python3 -m pip install -U python-can --user\
git clone https://gitlab.com/yann-v/grand-theft-simulator.git

# Launching the project
- first teminal\
cd s1_g4_damarius_jousslin_ville\
roscore

- second teminal\
cd s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6\
python3 scripts/game.py

# Display of the Can Bus
rostopic echo /can_bus

# Working functions
1. [x] Car moving with keyboard inputs
2. [x] Car headlights and blinks
3. [x] Map segmentation
    1. [x] Obstacle map
    2. [x] Cost map
4. [x] Detection around the car (Lidar)
5. [x] Car collision with restart screen
6. [x] Screens
    1. [x] Simulation screen (zoom x2)
    2. [x] Map unzoomed (mini map)
    3. [x] Costmap 
    4. [x] Dashboard
        1. [x] Acceleration
        2. [x] Velocity
        3. [x] Gear
        4. [x] Lights and blinks icons
    5. [x] Car with Lidar
7. [x] Pedestrian collision
8. [x] Autonomous police car
    1. [x] Follow the path until the collided pedestrian 
    2. [x] Follow the path until the user car
    3. [x] Collision with the car with busted screen
9. [x] Multiple maps
    1. [x] Strasboug Map
    2. [x] La Doua Map
    3. [x] Grenoble Map
10. [x] Menu
    1. [x] Car choice
    2. [x] Map choice
11. [ ] Music
    1. [x] Menu
    2. [ ] Game

# Vidéo de présentation

https://www.youtube.com/watch?v=HMGPxFHTmzo(url)

