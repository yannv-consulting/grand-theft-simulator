#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/devel:$CMAKE_PREFIX_PATH"
export CPATH="/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/devel/include:$CPATH"
export LD_LIBRARY_PATH="/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/devel/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/build"
export PYTHONPATH="/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src:$ROS_PACKAGE_PATH"