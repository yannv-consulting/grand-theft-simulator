#!/usr/bin/env python

import os
import pygame
from math import sin, radians, degrees, copysign
from pygame.math import Vector2
import numpy as np
import os
import pygame
from PIL import Image
import rospy
from geometry_msgs.msg import Pose2D
import map_segmentation

class Police_tf:
    def __init__(self, x, y, width, height):
        self.zoom_coef = 2
        self.scaling_factor = 32

        self.current_dir = os.path.dirname(os.path.abspath(__file__))
        self.police_tf_path = os.path.join(self.current_dir, "../src/vehicules/police.png")
        self.police_tf_image_init = pygame.image.load(self.police_tf_path)
        self.police_tf_image_init = pygame.transform.scale(self.police_tf_image_init, (self.zoom_coef * int(self.scaling_factor), self.zoom_coef * int(self.scaling_factor / 2)))
        self.police_tf_image = self.police_tf_image_init

        self.police_tf_coords = [x, y]
        self.screen = pygame.display.set_mode((width, height))

        self.pub_police_tf = rospy.Publisher('police_tf/position', Pose2D, queue_size=10)

        self.tempo_counter = 0
        self.move_counter = 0

    def position_publisher(self):
        pos = Pose2D()
        pos.x = self.police_tf_coords[0]
        pos.y = self.police_tf_coords[1]
        pos.theta = 0
        self.pub_police_tf.publish(pos)
        
    def move(self, path_list):
        if self.move_counter < len(path_list)-1:
            if path_list[self.move_counter + 1][0] > path_list[self.move_counter][0]:
                pass
            elif path_list[self.move_counter + 1][1] > path_list[self.move_counter][1]:
                self.police_tf_image = pygame.transform.rotate(self.police_tf_image_init, -90)
            elif path_list[self.move_counter + 1][0] < path_list[self.move_counter][0]:
                self.police_tf_image = pygame.transform.rotate(self.police_tf_image_init, 180)
            else:
                self.police_tf_image = pygame.transform.rotate(self.police_tf_image_init, 90)
            
            if self.tempo_counter == 10:
                self.move_counter += 1
                self.tempo_counter = 0
            else :
                self.tempo_counter += 1
            self.police_tf_coords = (path_list[self.move_counter][0],path_list[self.move_counter][1])
