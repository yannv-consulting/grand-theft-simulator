#!/usr/bin/env python

import os
import pygame
from math import sin, radians, degrees, copysign
from pygame.math import Vector2
import numpy as np
from PIL import Image
import queue
from shapely.geometry import Polygon
from itertools import cycle

import math
import menu
import pedestrian
import driving_system
import police_tf
import map_segmentation
import background
import path_thread
import pathfinding
import canbus

import rospy
from geometry_msgs.msg import Pose2D

from itertools import cycle
        
class Game:
    def __init__(self, choosed_car, choosed_map):
        pygame.init()

        self.clock = pygame.time.Clock()
        pygame.display.set_caption("Car simulation")
        
        self.car_image = choosed_car
        self.map_name = choosed_map
        segmentation = map_segmentation.Map(self.map_name)

        # Simulation variables
        self.ticks = 60
        self.exit = False
        self.brake_coef = 100
        self.zoom_coef = 2
        self.ppu = 32

         # Simulation parameters
        self.main_screen_limits = 3/4
        self.map_zoom = 1/2
        self.scaling_factor = 32
        self.zoom_path_coef = 2

        self.pixel_coef = segmentation.pixel_coef

        # Position paramters
        self.car_coords = (183, 603)
        self.police_car_coords = (183, 623)
        self.tf_police_coords = (-100, -100)
        self.pedestrian1_coords = (600, 228)
        self.pedestrian2_coords = (600, 588)
        self.pedestrian3_coords = (150, 588)
        self.pedestrian4_coords = (150, 228)

        crop_image_path = "src/maps/" + self.map_name + "/mapcrop.png"

        # If new map segmentation needed
        map_path = "src/maps/" + self.map_name + "/map.png"
        map_path = "src/maps/" + self.map_name + "/map.png"
        # crop_image_path = segmentation.image_to_crop_and_rgb_image_path(map_path)

        self.back_ground = background.Background(crop_image_path, [0, 0], self.zoom_coef)

        self.width = self.back_ground.rect.width
        self.height = self.back_ground.rect.height

        # Create the display area
        self.screen = pygame.display.set_mode((self.width, self.height))
        self.screen.fill((50, 50, 50))
        
        matrice_rgb = segmentation.crop_image_to_matrix_rgb(crop_image_path)
        self.matrice_seg = segmentation.matrix_segmentation_image(matrice_rgb)

        # If new cost map needed
        # self.black_white_low_resolution_path = segmentation.matrix_to_cost_map_image_path(self.matrice_seg)

        image_low_path = "src/maps/" + self.map_name + "/map_low.png"
        self.cost_matrix = segmentation.cost_map_image_path_to_cost_matrix(image_low_path)
        
         # If new rgba map needed
        # self.path_mask = segmentation.image_to_mask_image(self.matrice_seg)
        # self.path_mask = "src/maps/" + self.map_name + "/map_rgba.png"

        self.lights = False
        self.big_lights = False
        self.blink_right = False
        self.blink_left = False
        self.blink_increment = 0

        self.bool_police_auto = False

        # Path initialisation
        self.path = pathfinding.AStar(self.width, self.height, self.pixel_coef)
        self.my_queue = queue.Queue()

        # Create an event
        self._CLIGNOTER = pygame.USEREVENT + 1
        pygame.time.set_timer(self._CLIGNOTER, 80)

        self.next_pos_pub = rospy.Publisher('next_pos_tf', Pose2D, queue_size=10)    
        # rospy.init_node('next_pos_publisher', anonymous=True)

        self.start_police = False
        self.counter_police_path = 0

    def update(self, events) :
        for event in events :
            if event.type == self._CLIGNOTER :
                self.run()
                break
        
    def restart(self, color):
        self.screen.fill(color)
        self.lights = False
        self.big_lights = False
        self.blink_right = False
        self.blink_left = False
        car = driving_system.Car(self.car_coords[0] // self.ppu, self.car_coords[1] // self.ppu)
        return car
        
    def rotate(self, img, pos, angle):
        w, h = img.get_size()
        img2 = pygame.Surface((w * 2, h * 2), pygame.SRCALPHA)
        img2.blit(img, (w-pos[0], h-pos[1]))
        return pygame.transform.rotate(img2, angle)

    def check_collision_pedestrian(self, polygon1, polygon2, pedestrian_col, respawn_coords, police_car_coords, path_list):
        if polygon1.intersects(polygon2):
            if not self.start_police:
                pedestrian_center = pedestrian_col.pedestrian_image.get_rect().center
                pedestrian_center_coords = [pedestrian_col.pedestrian_coords[0] + pedestrian_center[0], pedestrian_col.pedestrian_coords[1] + pedestrian_center[1]]
                self.path_following = True

                # Launch the pathfinding
                self.path_threading = path_thread.PathThread(args = (self.path, [int(police_car_coords[0]), int(police_car_coords[1])], [int(pedestrian_center_coords[0]), int(pedestrian_center_coords[1])], self.cost_matrix, 
                    self.back_ground, self.screen, self.my_queue))
                self.path_threading.start()
                self.counter_police_path += 1
                self.searching_path = True
            pedestrian_col = pedestrian.Pedestrian(respawn_coords[0], respawn_coords[1], self.width, self.height)
            self.searching_path = True
            self.screen.blit(pedestrian_col.pedestrian_image, pedestrian_col.pedestrian_coords)
        return pedestrian_col

    def mask_creation(self, surface, surface_coords = None):
        surface_mask = pygame.mask.from_surface(surface)
        surface_mask_list = surface_mask.outline()
        surface_mask_list_real = []
        # Add the real coordinates from the surface
        for tuple in surface_mask_list:
            x = tuple[0] + surface_coords[0]
            y = tuple[1] + surface_coords[1]
            surface_mask_list_real.append((x,y))
        #pygame.draw.lines(self.screen, (255, 0, 0), 1, surface_mask_list_real)
        surface_polygon = Polygon(surface_mask_list_real)
        return surface_polygon

    def run(self):
        current_dir = os.path.dirname(os.path.abspath(__file__))
        car_image = self.car_image

        police_path = os.path.join(current_dir, "../src/vehicules/police.png")
        police_image = pygame.image.load(police_path)
        # self.police_image_rotated = pygame.transform.rotate(self.police_image, self.angle)

        # Colors Definition
        BLACK = (0, 0, 0)
        GREEN = (0, 255, 0)
        BLUE = (0, 0, 255)
        RED = (255, 0, 0)
        GREY = (50, 50, 50)
        BUILDING = (29, 44, 77)
        PINK = (255, 51, 204)

        # Objects Creation
        pedestrian1 = pedestrian.Pedestrian(self.pedestrian1_coords[0], self.pedestrian1_coords[1], self.width, self.height)
        pedestrian2 = pedestrian.Pedestrian(self.pedestrian2_coords[0], self.pedestrian2_coords[1], self.width, self.height)
        pedestrian3 = pedestrian.Pedestrian(self.pedestrian3_coords[0], self.pedestrian3_coords[1], self.width, self.height)
        pedestrian4 = pedestrian.Pedestrian(self.pedestrian4_coords[0], self.pedestrian4_coords[1], self.width, self.height)
        tf_police = police_tf.Police_tf(self.tf_police_coords[0],self.tf_police_coords[1], self.width, self.height)
        car = driving_system.Car(self.car_coords[0] // self.ppu, self.car_coords[1] // self.ppu)
        police_car = driving_system.Car(self.police_car_coords[0] // self.ppu, self.police_car_coords[1] // self.ppu)

        # Can Bus
        can_bus = canbus.Can()

        # Path initialisation
        path_list = []

        self.searching_path = False
        path_to_show = False

        # Pedestrian path
        pedestrian1_path = [1, 2, 3, 4]
        pedestrian2_path = [2, 3, 4, 1]
        pedestrian3_path = [3, 4, 1, 2]
        pedestrian4_path = [4, 1, 2, 3]

        # Load dashboard
        dashboard_image = pygame.image.load("src/dashboard/dashboard.png")
        acc_pointer = pygame.image.load("src/dashboard/acc_pointer.png")
        vel_pointer = pygame.image.load("src/dashboard/vel_pointer.png")
        lights_image = pygame.image.load("src/dashboard/low_headlight.png")
        big_lights_image = pygame.image.load("src/dashboard/high_headlight.png")
        blink_left_image = pygame.image.load("src/dashboard/blink_left_icon.png")
        blink_right_image = pygame.image.load("src/dashboard/blink_right_icon.png")

        dashboard_gears = []
        for i in range(6):
            dashboard_gear = pygame.image.load("src/dashboard/gear_" + str(i + 1) + ".png")
            dashboard_gear = pygame.transform.scale(dashboard_gear, (dashboard_gear.get_rect().width // 3, dashboard_gear.get_rect().height // 3))
            dashboard_gears.append(dashboard_gear)

        dashboard_image = pygame.transform.scale(dashboard_image, (dashboard_image.get_rect().width // 3, dashboard_image.get_rect().height // 3))
        lights_image = pygame.transform.scale(lights_image, (lights_image.get_rect().width // 3, lights_image.get_rect().height // 3))
        big_lights_image = pygame.transform.scale(big_lights_image, (big_lights_image.get_rect().width // 3, big_lights_image.get_rect().height // 3))
        blink_left_image = pygame.transform.scale(blink_left_image, (blink_left_image.get_rect().width // 3, blink_left_image.get_rect().height // 3))
        blink_right_image = pygame.transform.scale(blink_right_image, (blink_right_image.get_rect().width // 3, blink_right_image.get_rect().height // 3))
        acc_pointer = pygame.transform.scale(acc_pointer, (acc_pointer.get_rect().width // 3, acc_pointer.get_rect().height // 3))
        vel_pointer = pygame.transform.scale(vel_pointer, (vel_pointer.get_rect().width // 3, vel_pointer.get_rect().height // 3))

        while not self.exit:       
            
            # Publishers
            car.position_publisher()
            pedestrian1.position_publisher()
            tf_police.position_publisher()
            police_car.position_publisher()
            car.position_publisher()
            
            dt = self.clock.get_time() / 1000

            # Event queue
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_l:
                       self.lights = not(self.lights)
                       self.big_lights = False
                    if event.key == pygame.K_m:
                       self.big_lights = not(self.big_lights)
                       self.lights = False
                    if event.key == pygame.K_j:
                       self.blink_left = not(self.blink_left)
                       self.blink_right = False
                    if event.key == pygame.K_k:
                       self.blink_right = not(self.blink_right)
                       self.blink_left = False
                       
                if event.type == pygame.QUIT:
                    self.exit = True

            # User input
            pressed = pygame.key.get_pressed()
            if pressed[pygame.K_p]:
                car = self.restart(GREY)
            
            car.command(pressed, dt)
            police_car.command_police(dt)
            
            # Pedestrian movement
            pedestrian1.movement(pedestrian1_path)
            pedestrian2.movement(pedestrian2_path)
            pedestrian3.movement(pedestrian3_path)
            pedestrian4.movement(pedestrian4_path)
            
            # Front Lightning
            car_light = driving_system.Lightning()
            lights, arbitration_id_light, data_light = car_light.lightDisplay(self.lights, self.big_lights) #, light_command, big_light_command)
            lights = pygame.transform.scale(lights, (self.zoom_coef * int(self.scaling_factor), self.zoom_coef * int(self.scaling_factor / 2)))
            rotated_lights = pygame.transform.rotate(lights, car.angle)

            # Blink
            blinks, arbitration_id_blink, data_blink = car_light.blinkDisplay(self.blink_right, self.blink_left, self.blink_increment)#, blink_left_command, blink_right_command)
            self.blink_increment += 1
            if self.blink_increment > 10:
                self.blink_increment = 0
            blinks = pygame.transform.scale(blinks, (self.zoom_coef * int(self.scaling_factor), self.zoom_coef * int(self.scaling_factor / 2)))
            rotated_blinks = pygame.transform.rotate(blinks, car.angle)
            
            # Resize and rotate the cars
            car_image = pygame.transform.scale(car_image, (self.zoom_coef * int(self.scaling_factor), self.zoom_coef * int(self.scaling_factor / 2)))
            police_image = pygame.transform.scale(police_image, (self.zoom_coef * int(self.scaling_factor), self.zoom_coef * int(self.scaling_factor / 2)))

            rotated_car = pygame.transform.rotate(car_image, car.angle)
            police_rotated_car = pygame.transform.rotate(police_image, police_car.angle)

            # Cars coordinates
            car_coords = car.position * self.ppu - (rotated_car.get_rect().width / 2, rotated_car.get_rect().height / 2)
            police_car_coords = police_car.position * self.ppu - (police_rotated_car.get_rect().width / 2, police_rotated_car.get_rect().height / 2)

            # Cars center coords
            car_center = rotated_car.get_rect().center
            car_center_coords = [int(car_coords[0] + car_center[0]), int(car_coords[1] + car_center[1])]
            police_center = police_image.get_rect().center
            police_center_coords = [police_car_coords[0] + police_center[0], police_car_coords[1] + police_center[1]]

            # Calculate collisions
            bool_collision = car.check_collision(car_center_coords, self.matrice_seg)

            # Initialise the background object
            self.back_ground.image = pygame.transform.scale(self.back_ground.image, (self.width, self.height))
            self.screen.blit(self.back_ground.image, self.back_ground.rect)

            # Do something if collision detected
            if bool_collision:
                self.back_ground.restart_screen(self.screen)
                car = self.restart(GREY)
            
            police_center = police_image.get_rect().center
            police_center_coords = [police_car_coords[0] + police_center[0], police_car_coords[1] + police_center[1]]
            police_center_coords_bis = [police_car_coords[0] - police_center[0], police_car_coords[1] - police_center[1]]

            car.set_max_acceleration(car_center_coords, self.matrice_seg)
            police_car.set_max_acceleration(police_center_coords, self.matrice_seg)
            
            # Display the path on the screen
            if path_to_show or (self.searching_path and not self.path_threading.isAlive()):
                if not path_list:
                    path_list = self.my_queue.get()
                    self.searching_path = False
                for node in path_list:
                    pygame.draw.rect(self.screen, GREEN, (node[0] - 3, node[1] - 3, 6, 6))
                pygame.draw.rect(self.screen, RED, (path_list[0][0] - 3, path_list[0][1] - 3, 6, 6))
                path_to_show = True

            #Ranger function
            ranger = driving_system.Ranger()

            police_car_angle = police_car.angle * math.pi / 180
            laser_police = ranger.update(police_center_coords_bis, police_car_angle, self.matrice_seg, self.width, self.height)

            # Angle conversion
            car_angle = car.angle * math.pi / 180

            laser = ranger.update(car_center_coords, car_angle, self.matrice_seg, self.width, self.height)
            path_available_angle = []
            path_available_angle = car.auto_drive(laser, dt)

            if path_to_show:
                if not self.start_police:
                    police_car.police_init_angle(path_list)
                    self.start_police = True
                police_car.path_following(path_list, police_car_coords, dt)
                #police_car.auto_drive(laser_police, dt2)
            
            if self.start_police and path_list == []:
                path_to_show = False
                self.searching_path = False
                self.start_police = False
                if self.counter_police_path > 0:
                    self.path_following = True
                    # Launch the pathfinding
                    self.path_threading = path_thread.PathThread(args = (self.path, [int(police_car_coords[0]), int(police_car_coords[1])], 
                        [int(car_center_coords[0]), int(car_center_coords[1])], self.cost_matrix, self.back_ground, self.screen, self.my_queue))
                    self.path_threading.start()
                    self.counter_police_path += 1
                    self.searching_path = True

             # Car dt update
            car.update(dt)   
            police_car.update(dt)

            # Can Bus Velocity, Acceleration, Steering
            arbitration_id_velocity = car.arbitration_id_velocity
            arbitration_id_acceleration = car.arbitration_id_acceleration
            arbitration_id_steering = car.arbitration_id_steering
            
            # Velocity between 0 and 140 km/h
            data_velocity = abs(int(car.velocity.x * 17.5))
            
            # Acceleration in RPM / 100
            data_acceleration = abs(int(car.acceleration * 5))

            # Steering value between 0 and 160, centered at 80
            data_steering =  abs(int(car.steering) + 80)

            # Can Bus Display
            can_bus.send_message(arbitration_id_velocity, data_velocity)
            can_bus.send_message(arbitration_id_acceleration, data_acceleration)
            can_bus.send_message(arbitration_id_steering, data_steering)
            can_bus.send_message(arbitration_id_light, data_light)
            can_bus.send_message(arbitration_id_blink, data_blink)
            
            for x in laser_police:
                pygame.draw.rect(self.screen, GREEN, (x[0], x[1], 2, 2))
            
             # Car, lights, pedestrian and police displays
            self.screen.blit(rotated_car, car_coords)
            self.screen.blit(rotated_lights, car_coords)
            self.screen.blit(rotated_blinks, car_coords)
            self.screen.blit(pedestrian1.pedestrian_image, pedestrian1.pedestrian_coords)
            self.screen.blit(pedestrian2.pedestrian_image, pedestrian2.pedestrian_coords)
            self.screen.blit(pedestrian3.pedestrian_image, pedestrian3.pedestrian_coords)
            self.screen.blit(pedestrian4.pedestrian_image, pedestrian4.pedestrian_coords)
            self.screen.blit(police_rotated_car, police_center_coords_bis)

            # Mask creation of the different objects
            car_polygon = self.mask_creation(rotated_car, car_coords)
            pedestrian1_polygon = self.mask_creation(pedestrian1.pedestrian_image, pedestrian1.pedestrian_coords)
            pedestrian2_polygon = self.mask_creation(pedestrian2.pedestrian_image, pedestrian2.pedestrian_coords)
            pedestrian3_polygon = self.mask_creation(pedestrian3.pedestrian_image, pedestrian3.pedestrian_coords)
            pedestrian4_polygon = self.mask_creation(pedestrian4.pedestrian_image, pedestrian4.pedestrian_coords)
            police_polygon = self.mask_creation(police_image, police_center_coords_bis)
            
            # Mask creation of the map (too slow)
            # mask_map = pygame.image.load(self.path_mask)
            # map_mask = pygame.mask.from_surface(mask_map)
            # map_masks = map_mask.connected_components()
            # map_mask_list = map_mask.outline()
            # pygame.draw.lines(self.screen, RED, 1, map_mask_list)
            # map_polygon = Polygon(map_mask_list)
            # for mask in map_masks:
            #     map_outline = mask.outline()
            #     try:
            #         pygame.draw.lines(self.screen, RED, 1, map_outline)
            #         map_polygon = Polygon(map_outline)
            #     except ValueError:
            #         pass

            # Collision
            pedestrian1 = self.check_collision_pedestrian(car_polygon, pedestrian1_polygon, pedestrian1, self.pedestrian1_coords, police_center_coords, self.searching_path)
            pedestrian2 = self.check_collision_pedestrian(car_polygon, pedestrian2_polygon, pedestrian2, self.pedestrian2_coords, police_center_coords, self.searching_path)
            pedestrian3 = self.check_collision_pedestrian(car_polygon, pedestrian3_polygon, pedestrian3, self.pedestrian3_coords, police_center_coords, self.searching_path)
            pedestrian4 = self.check_collision_pedestrian(car_polygon, pedestrian4_polygon, pedestrian4, self.pedestrian4_coords, police_center_coords, self.searching_path)

            ###################################################



            # if self.bool_police_auto:
            #     police_car.auto_drive(laser_police, dt2)
            #################################################

            # Duplicate the screen before displaying it
            simulation_screen = self.screen.copy()
            map_screen = self.screen.copy()

            # Reset the main screen
            self.screen.fill(GREY)

            if car_polygon.intersects(police_polygon):
                self.back_ground.busted_screen(self.screen)
                car = self.restart(GREY)

            # Dashbord
            # Max velocity 8.0, max acc 5.771
            # 20 deg for 1000 rpm/min
            velocity = abs(car.velocity.x)
            angle_acc = - abs(car.acceleration) * 15
            
            if angle_acc == - 150:
                angle_acc = 0

            if velocity < 1.0:
                gear_image = dashboard_gears[0]
            elif velocity < 2.0:
                gear_image = dashboard_gears[1]
            elif velocity < 3.0:
                gear_image = dashboard_gears[2]
            elif velocity < 4.0:
                gear_image = dashboard_gears[3]
            elif velocity < 5.0:
                gear_image = dashboard_gears[4]
            else:
                gear_image = dashboard_gears[5]

            angle_vel = - velocity * 17.5

            dashboard_car = pygame.transform.rotate(car_image, 90)
            x_dashboard_car = int(7 / 8 * self.width)
            y_dashboard_car = int((1 - 1 / 3) * self.height)
            self.screen.blit(dashboard_car, (x_dashboard_car, y_dashboard_car))

            #Lidar displays
            for x in laser:
                if x[3] > 40:
                    pygame.draw.rect(self.screen, GREEN, (x_dashboard_car + car_center[0] / 2 + x[3] * math.cos(math.radians(x[2] + 90)), 
                        y_dashboard_car + car_center[0] + x[3] * math.sin(math.radians(x[2] - 90)), 2, 2))
                else:
                    pygame.draw.rect(self.screen, RED, (x_dashboard_car + car_center[0] / 2 + x[3] * math.cos(math.radians(x[2] + 90)), 
                        y_dashboard_car + car_center[1] + x[3] * math.sin(math.radians(x[2] - 90)), 2, 2))
            
            pygame.draw.circle(self.screen, RED, (x_dashboard_car + car_center[0] // 2, y_dashboard_car + car_image.get_rect().height), 40, 1)
            pygame.draw.circle(self.screen, GREEN, (x_dashboard_car + car_center[0] // 2, y_dashboard_car + car_image.get_rect().height), 100, 1)        

            rotated_acc_pointer = pygame.transform.rotozoom(acc_pointer, angle_acc, 1)
            rotated_vel_pointer = pygame.transform.rotozoom(vel_pointer, angle_vel, 1)

            x_dashboard = int(3 / 4 * self.width) + 50
            y_dashboard = y_dashboard_car + 150

            center_acc_pointer = (acc_pointer.get_rect().centerx + x_dashboard + 34, acc_pointer.get_rect().centery + y_dashboard + 80)
            rect_acc_pointer = rotated_acc_pointer.get_rect(center = center_acc_pointer)

            center_vel_pointer = (vel_pointer.get_rect().centerx + x_dashboard + 174, vel_pointer.get_rect().centery + y_dashboard + 56)
            rect_vel_pointer = rotated_vel_pointer.get_rect(center = center_vel_pointer)

            self.screen.blit(dashboard_image, (x_dashboard, y_dashboard))
            self.screen.blit(gear_image, (x_dashboard, y_dashboard))
            self.screen.blit(rotated_acc_pointer, rect_acc_pointer)
            self.screen.blit(rotated_vel_pointer, rect_vel_pointer)

            if self.lights:
                self.screen.blit(lights_image, (x_dashboard, y_dashboard))

            if self.big_lights:
                self.screen.blit(big_lights_image, (x_dashboard, y_dashboard))

            if self.blink_left:
                self.screen.blit(blink_left_image, (x_dashboard, y_dashboard))

            if self.blink_right:
                self.screen.blit(blink_right_image, (x_dashboard, y_dashboard))

            # Scale the simulation screen
            x_zoom_simu = self.width * self.zoom_coef
            y_zoom_simu = self.height * self.zoom_coef
            
            # Calculate the simulation screen parameters
            width_simu = self.width * self.main_screen_limits
            height_simu = self.height
            x_top_simu = int(car_coords[0] * self.zoom_coef + car_center[0] - width_simu / 2)
            y_top_simu = int(car_coords[1] * self.zoom_coef + car_center[1] - height_simu / 2)

            # Display the simulation screen
            pygame.draw.rect(self.screen, BUILDING,(0, 0, width_simu, height_simu))
            simulation_screen = pygame.transform.scale(simulation_screen, (int(x_zoom_simu), int(y_zoom_simu)))
            self.screen.blit(simulation_screen, (0, 0), (x_top_simu, y_top_simu, width_simu, height_simu))
            
            # Scale the map screen
            x_zoom_map = int(self.width * self.map_zoom)
            y_zoom_map = int(self.height * self.map_zoom)
            map_screen = pygame.transform.scale(map_screen, (x_zoom_map, y_zoom_map))

            # Calculate the map screen parameters
            width_map = (1 - self.main_screen_limits) * self.width
            height_map = self.height / 3
            x_top_map = int(car_coords[0] * self.map_zoom + car_center[0] - width_map / 2)
            y_top_map = int(car_coords[1] * self.map_zoom + car_center[1] - height_map / 2)

            # Display the map screen
            pygame.draw.rect(self.screen, BUILDING, (self.width * self.main_screen_limits, 0, width_map, height_map))
            self.screen.blit(map_screen, (width_simu, 0), (x_top_map, y_top_map, width_map, height_map))

            # Debug for pathfinding
            pathfinding_image = pygame.image.load("src/maps/" + self.map_name + "/map_low.png")
            rect_path_image = pathfinding_image.get_rect()
            pathfinding_image = pygame.transform.scale(pathfinding_image, (rect_path_image.width * self.zoom_path_coef, rect_path_image.height * self.zoom_path_coef))
            self.screen.blit(pathfinding_image, (width_simu, self.height / 3))

            if path_to_show:
                if path_list != []:
                    for node in path_list:
                        pygame.draw.rect(self.screen, PINK, (width_simu + node[0] * self.zoom_path_coef // self.pixel_coef, self.height / 3 + node[1] * 2 // self.pixel_coef, 2, 2))
                    pygame.draw.rect(self.screen, BLACK, (width_simu + path_list[0][0] * self.zoom_path_coef // self.pixel_coef, self.height / 3 + path_list[0][1] * 2 // self.pixel_coef, 2, 2))
                else:
                    path_to_show = False
            # End of the main loop
            pygame.display.flip()
            self.clock.tick(self.ticks)
        pygame.quit()

if __name__ == '__main__':
    app = menu.Application()
    app.menu()
    clock = pygame.time.Clock()
 
    while app.statut :
      app.update()
      clock.tick(30)
