#!/usr/bin/env python

import pygame
import game
import os

MenuW = 800 #Window's dimension / Width
MenuH = 600 #Window's dimension / Height

class Menu :
    """ Création et gestion des boutons d'un menu """
    def __init__(self, application, *groupes) :
        self.couleurs = dict(
            normal=(0, 200, 0),
            survol=(0, 200, 200),
        )
        font = pygame.font.SysFont('Helvetica', 24, bold=True)
        # Name of then buttons available and their associated command
        items = (
            # ('Start Simulation', application.jeu),
            ('Car Selection', application.selection_car),
            ('QUIT', application.quitter)
        )
        x = 400
        y = 200
        self._boutons = []
        for texte, cmd in items :
            mb = MenuBouton(texte, self.couleurs['normal'], font, x, y, 200, 50, cmd)
            self._boutons.append(mb)
            y += 120
            for groupe in groupes :
                groupe.add(mb)
 
    def update(self, events) :
        clicGauche, *_ = pygame.mouse.get_pressed()
        posPointeur = pygame.mouse.get_pos()
        for bouton in self._boutons :
            # If mouse on a button
            if bouton.rect.collidepoint(*posPointeur) :
                # Change of the cursor
                pygame.mouse.set_cursor(*pygame.cursors.tri_left)
                # Change of the button's color
                bouton.dessiner(self.couleurs['survol'])
                # If left clic
                if clicGauche :
                    # Call of the function
                    bouton.executerCommande()
                break
            else :
                # The mouse is not on the button
                bouton.dessiner(self.couleurs['normal'])
        else :
            # Initialisation of the default cursor
            pygame.mouse.set_cursor(*pygame.cursors.arrow)

    def detruire(self) :
        # Initialisation of the default cursor
        pygame.mouse.set_cursor(*pygame.cursors.arrow)
 
class MenuBouton(pygame.sprite.Sprite) :
    """ Creation of a rectangle button """
    def __init__(self, texte, couleur, font, x, y, largeur, hauteur, commande) :
        super().__init__()
        self._commande = commande
 
        self.image = pygame.Surface((largeur, hauteur))
 
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)
 
        self.texte = font.render(texte, True, (0, 0, 0))
        self.rectTexte = self.texte.get_rect()
        self.rectTexte.center = (largeur/2, hauteur/2)
 
        self.dessiner(couleur)
 
    def dessiner(self, couleur) :
        self.image.fill(couleur)
        self.image.blit(self.texte, self.rectTexte)
 
    def executerCommande(self) :
        # Call of the function
        self._commande()

class Application :
    """ Main class managing different innterfaces """
    def __init__(self) :
        self.menu_selection = 0
        pygame.init()
        pygame.display.set_caption("Main Menu")

        # Display variable
        self.clock = pygame.time.Clock()
        self.ticks = 30

        self.current_dir = os.path.dirname(os.path.abspath(__file__))
 
        self.fond = (150,)*3
        self.compteur_car = 0
        self.compteur_map = 0

        self.choosed_car = 0

        self.font = pygame.font.SysFont('Helvetica', 24, bold=True)
 
        self.fenetre = pygame.display.set_mode((MenuW,MenuH))
        # Group of Sprites used for the display
        self.groupe_global = pygame.sprite.Group()
        self.statut = True
 
    def _initialiser(self) :
        try:
            self.ecran.detruire()
            # Deletion of all Sprites
            self.groupe_global.empty()
        except AttributeError:
            pass
 
    def menu(self) :
        # Display of the menu
        self._initialiser()
        self.music()
        self.ecran = Menu(self, self.groupe_global)
        
    def jeu(self, choosed_car, choosed_map) :
        # Display of the game
        self._initialiser()
        pygame.mixer.music.stop()
        self.ecran = game.Game(choosed_car, choosed_map)
        
    def music(self) :
        # Initialisation of the menu's music
        pygame.mixer.init()
        pygame.mixer.music.load("src/musics/Rapido.wav")
        pygame.mixer.music.play(-1)
    
    def selection_car(self):
        # Function to select car layout
        self.menu_selection = 1
        
        car1_path = os.path.join(self.current_dir, "../src/vehicules/car_blue.png")
        car2_path = os.path.join(self.current_dir, "../src/vehicules/car_green.png")
        car3_path = os.path.join(self.current_dir, "../src/vehicules/car_red.png")
        car4_path = os.path.join(self.current_dir, "../src/vehicules/car_yellow.png")

        car1_image = pygame.image.load(car1_path)
        car2_image = pygame.image.load(car2_path)
        car3_image = pygame.image.load(car3_path)
        car4_image = pygame.image.load(car4_path)

        rect_big = car1_image.get_rect()
        rect_big.center =  (MenuW / 2, MenuH / 2)

        car_images = [car1_image, car2_image, car3_image, car4_image]

        car1_image = pygame.transform.scale(car1_image, (256, 128))
        car2_image = pygame.transform.scale(car2_image, (256, 128))
        car3_image = pygame.transform.scale(car3_image, (256, 128))
        car4_image = pygame.transform.scale(car4_image, (256, 128))

        rect_small1 = car1_image.get_rect()
        rect_small1.center = (MenuW / 5, MenuH / 2)
        rect_small2 = car1_image.get_rect()
        rect_small2.center = (4 * MenuW / 5, MenuH / 2)

        car_images_scaled = [car1_image, car2_image, car3_image, car4_image]

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT:
                    self.compteur_car += 1
                if event.key == pygame.K_LEFT:
                    self.compteur_car -= 1

        self._initialiser()
        self.fenetre.fill(self.fond)

        if self.compteur_car % 4 == 0:
            self.choosed_car = car1_image
            self.fenetre.blit(car_images_scaled[3], rect_small1)
            self.fenetre.blit(car_images[0], rect_big)
            self.fenetre.blit(car_images_scaled[1], rect_small2)
        elif self.compteur_car % 4 == 1:
            self.choosed_car = car2_image
            self.fenetre.blit(car_images_scaled[0], rect_small1)
            self.fenetre.blit(car_images[1], rect_big)
            self.fenetre.blit(car_images_scaled[2], rect_small2)
        elif self.compteur_car % 4 == 2:
            self.choosed_car = car3_image
            self.fenetre.blit(car_images_scaled[1], rect_small1)
            self.fenetre.blit(car_images[2], rect_big)
            self.fenetre.blit(car_images_scaled[3], rect_small2)
        elif self.compteur_car % 4 == 3:
            self.choosed_car = car4_image
            self.fenetre.blit(car_images_scaled[2], rect_small1)
            self.fenetre.blit(car_images[3], rect_big)
            self.fenetre.blit(car_images_scaled[0], rect_small2)
 
        image_bouton = pygame.Surface((300, 50))
 
        rect = image_bouton.get_rect()
        rect.center = (400, 500)

        head_text = self.font.render('Choose a car with the arrow keys', True, (0, 0, 0))
        head_rect_text = head_text.get_rect()
        head_rect_text.center = (rect[0] + rect[2] // 2, 100)

        texte = self.font.render('Map Selection', True, (0, 0, 0))
        rectTexte = texte.get_rect()
        rectTexte.center = (rect[0] + rect[2] // 2, 500)
        image_bouton.fill((0, 200, 0))

        self.fenetre.blit(image_bouton, rect)
        self.fenetre.blit(head_text, head_rect_text)
        self.fenetre.blit(texte, rectTexte)

        clicGauche, *_ = pygame.mouse.get_pressed()
        posPointeur = pygame.mouse.get_pos()

        if rect.collidepoint(*posPointeur):
            pygame.mouse.set_cursor(*pygame.cursors.tri_left)
            image_bouton.fill((0, 200, 200))
            image_bouton.blit(texte, rectTexte)
            self.fenetre.blit(image_bouton, rect)
            self.fenetre.blit(texte, rectTexte)
            if clicGauche:
                self.selection_map()
            else:
                image_bouton.fill((0, 200, 0))
                image_bouton.blit(texte, rectTexte)

    def selection_map(self):
        # Function to select the map
        self.menu_selection = 2

        map1_path = os.path.join(self.current_dir, "../src/maps/Doua/mapcrop.png")
        map2_path = os.path.join(self.current_dir, "../src/maps/Stras/map.png")
        map3_path = os.path.join(self.current_dir, "../src/maps/Grenoble/map.png")

        map1_image = pygame.image.load(map1_path)
        map2_image = pygame.image.load(map2_path)
        map3_image = pygame.image.load(map3_path)

        map1_image = pygame.transform.scale(map1_image, (318, 180))
        map2_image = pygame.transform.scale(map2_image, (318, 180))
        map3_image = pygame.transform.scale(map3_image, (318, 180))

        rect_big = map1_image.get_rect()
        rect_big.center =  (MenuW / 2, MenuH / 2)

        map_images = [map1_image, map2_image, map3_image]

        map1_image = pygame.transform.scale(map1_image, (212, 120))
        map2_image = pygame.transform.scale(map2_image, (212, 120))
        map3_image = pygame.transform.scale(map3_image, (212, 120))

        rect_small1 = map1_image.get_rect()
        rect_small1.center = (MenuW / 5, MenuH / 2)
        rect_small2 = map1_image.get_rect()
        rect_small2.center = (4 * MenuW / 5, MenuH / 2)

        map_images_scaled = [map1_image, map2_image, map3_image]


        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT:
                    self.compteur_map += 1
                if event.key == pygame.K_LEFT:
                    self.compteur_map -= 1

        self._initialiser()
        self.fenetre.fill(self.fond)

        if self.compteur_map % 3 == 0:
            choosed_map = "Doua"
            self.fenetre.blit(map_images_scaled[2], rect_small1)
            self.fenetre.blit(map_images_scaled[1], rect_small2)
            self.fenetre.blit(map_images[0], rect_big)
        elif self.compteur_map % 3 == 1:
            choosed_map = "Stras"
            self.fenetre.blit(map_images_scaled[0], rect_small1)
            self.fenetre.blit(map_images_scaled[2], rect_small2)
            self.fenetre.blit(map_images[1], rect_big)
        elif self.compteur_map % 3 == 2:
            choosed_map = "Grenoble"
            self.fenetre.blit(map_images_scaled[1], rect_small1)
            self.fenetre.blit(map_images_scaled[0], rect_small2)
            self.fenetre.blit(map_images[2], rect_big)

        image_bouton = pygame.Surface((300, 50))
 
        rect = image_bouton.get_rect()
        rect.center = (400, 500)

        head_text = self.font.render('Choose a map with the arrow keys', True, (0, 0, 0))
        head_rect_text = head_text.get_rect()
        head_rect_text.center = (rect[0] + rect[2] // 2, 100)
 
        texte = self.font.render('Start Simulation', True, (0, 0, 0))
        rectTexte = texte.get_rect()
        rectTexte.center = (rect[0] + rect[2] // 2, 500)
        image_bouton.fill((0, 200, 0))

        self.fenetre.blit(image_bouton, rect)
        self.fenetre.blit(head_text, head_rect_text)
        self.fenetre.blit(texte, rectTexte)

        clicGauche, *_ = pygame.mouse.get_pressed()
        posPointeur = pygame.mouse.get_pos()

        if rect.collidepoint(*posPointeur):
            pygame.mouse.set_cursor(*pygame.cursors.tri_left)
            image_bouton.fill((0, 200, 200))
            image_bouton.blit(texte, rectTexte)
            self.fenetre.blit(image_bouton, rect)
            self.fenetre.blit(texte, rectTexte)
            if clicGauche:
                self.fenetre.fill(self.fond)
                self.jeu(self.choosed_car, choosed_map)
        else:
            image_bouton.fill((0, 200, 0))
            image_bouton.blit(texte, rectTexte)

    def quitter(self) :
        self.statut = False
 
    def update(self) :
        events = pygame.event.get()
 
        for event in events :
            if event.type == pygame.QUIT :
                self.quitter()
                return
 
        if self.menu_selection == 0:
            self.fenetre.fill(self.fond)
            self.ecran.update(events)
            self.groupe_global.update()
            self.groupe_global.draw(self.fenetre)
            pygame.display.update()
        elif self.menu_selection == 1:
            self.selection_car()
            self.ecran.update(events)
        elif self.menu_selection == 2:
            self.selection_map()
            self.ecran.update(events)
        else:
            self.ecran.update(events)

        pygame.display.flip()
        self.clock.tick(self.ticks)
