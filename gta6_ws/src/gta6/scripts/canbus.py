import can
from std_msgs.msg import String
import rospy

class Can:
    def __init__(self):
        # Definition of a virtual can bus
        self.bus1 = can.interface.Bus('BUS', bustype='virtual')
        self.bus2 = can.interface.Bus('BUS', bustype='virtual')

        self.pub = rospy.Publisher('can_bus', String, queue_size=10)

    def can_publisher(self, msg):
        # Send message in /can_bus ros topic
        self.pub.publish(str(msg))


    def send_message(self, t1, t2):
        msg1 = can.Message(arbitration_id=t1, data=t2)
        self.bus1.send(msg1)

        msg2 = self.bus2.recv()
        self.can_publisher(msg2)

