# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "gta6: 1 messages, 2 services")

set(MSG_I_FLAGS "-Igta6:/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/msg;-Igeometry_msgs:/opt/ros/indigo/share/geometry_msgs/cmake/../msg;-Istd_msgs:/opt/ros/indigo/share/std_msgs/cmake/../msg;-Inav_msgs:/opt/ros/indigo/share/nav_msgs/cmake/../msg;-Iactionlib_msgs:/opt/ros/indigo/share/actionlib_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(genlisp REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(gta6_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/msg/goalMsg.msg" NAME_WE)
add_custom_target(_gta6_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "gta6" "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/msg/goalMsg.msg" "geometry_msgs/Pose2D"
)

get_filename_component(_filename "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/localGoal.srv" NAME_WE)
add_custom_target(_gta6_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "gta6" "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/localGoal.srv" "std_msgs/Bool:geometry_msgs/Pose2D"
)

get_filename_component(_filename "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/Path.srv" NAME_WE)
add_custom_target(_gta6_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "gta6" "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/Path.srv" "geometry_msgs/Point:std_msgs/Header:geometry_msgs/Quaternion:geometry_msgs/PoseStamped:nav_msgs/Path:std_msgs/Bool:geometry_msgs/Pose"
)

#
#  langs = gencpp;genlisp;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(gta6
  "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/msg/goalMsg.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Pose2D.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gta6
)

### Generating Services
_generate_srv_cpp(gta6
  "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/localGoal.srv"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Bool.msg;/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Pose2D.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gta6
)
_generate_srv_cpp(gta6
  "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/Path.srv"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/indigo/share/geometry_msgs/cmake/../msg/PoseStamped.msg;/opt/ros/indigo/share/nav_msgs/cmake/../msg/Path.msg;/opt/ros/indigo/share/std_msgs/cmake/../msg/Bool.msg;/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Pose.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gta6
)

### Generating Module File
_generate_module_cpp(gta6
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gta6
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(gta6_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(gta6_generate_messages gta6_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/msg/goalMsg.msg" NAME_WE)
add_dependencies(gta6_generate_messages_cpp _gta6_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/localGoal.srv" NAME_WE)
add_dependencies(gta6_generate_messages_cpp _gta6_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/Path.srv" NAME_WE)
add_dependencies(gta6_generate_messages_cpp _gta6_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(gta6_gencpp)
add_dependencies(gta6_gencpp gta6_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS gta6_generate_messages_cpp)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(gta6
  "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/msg/goalMsg.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Pose2D.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gta6
)

### Generating Services
_generate_srv_lisp(gta6
  "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/localGoal.srv"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Bool.msg;/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Pose2D.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gta6
)
_generate_srv_lisp(gta6
  "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/Path.srv"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/indigo/share/geometry_msgs/cmake/../msg/PoseStamped.msg;/opt/ros/indigo/share/nav_msgs/cmake/../msg/Path.msg;/opt/ros/indigo/share/std_msgs/cmake/../msg/Bool.msg;/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Pose.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gta6
)

### Generating Module File
_generate_module_lisp(gta6
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gta6
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(gta6_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(gta6_generate_messages gta6_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/msg/goalMsg.msg" NAME_WE)
add_dependencies(gta6_generate_messages_lisp _gta6_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/localGoal.srv" NAME_WE)
add_dependencies(gta6_generate_messages_lisp _gta6_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/Path.srv" NAME_WE)
add_dependencies(gta6_generate_messages_lisp _gta6_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(gta6_genlisp)
add_dependencies(gta6_genlisp gta6_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS gta6_generate_messages_lisp)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(gta6
  "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/msg/goalMsg.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Pose2D.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gta6
)

### Generating Services
_generate_srv_py(gta6
  "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/localGoal.srv"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Bool.msg;/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Pose2D.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gta6
)
_generate_srv_py(gta6
  "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/Path.srv"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Point.msg;/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg;/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/indigo/share/geometry_msgs/cmake/../msg/PoseStamped.msg;/opt/ros/indigo/share/nav_msgs/cmake/../msg/Path.msg;/opt/ros/indigo/share/std_msgs/cmake/../msg/Bool.msg;/opt/ros/indigo/share/geometry_msgs/cmake/../msg/Pose.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gta6
)

### Generating Module File
_generate_module_py(gta6
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gta6
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(gta6_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(gta6_generate_messages gta6_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/msg/goalMsg.msg" NAME_WE)
add_dependencies(gta6_generate_messages_py _gta6_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/localGoal.srv" NAME_WE)
add_dependencies(gta6_generate_messages_py _gta6_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/fs03/share/users/aubin.jousselin/home/s1_g4_damarius_jousslin_ville/gta6_ws/src/gta6/srv/Path.srv" NAME_WE)
add_dependencies(gta6_generate_messages_py _gta6_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(gta6_genpy)
add_dependencies(gta6_genpy gta6_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS gta6_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gta6)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/gta6
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_cpp)
  add_dependencies(gta6_generate_messages_cpp geometry_msgs_generate_messages_cpp)
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(gta6_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()
if(TARGET nav_msgs_generate_messages_cpp)
  add_dependencies(gta6_generate_messages_cpp nav_msgs_generate_messages_cpp)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gta6)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/gta6
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_lisp)
  add_dependencies(gta6_generate_messages_lisp geometry_msgs_generate_messages_lisp)
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(gta6_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()
if(TARGET nav_msgs_generate_messages_lisp)
  add_dependencies(gta6_generate_messages_lisp nav_msgs_generate_messages_lisp)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gta6)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gta6\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/gta6
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_py)
  add_dependencies(gta6_generate_messages_py geometry_msgs_generate_messages_py)
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(gta6_generate_messages_py std_msgs_generate_messages_py)
endif()
if(TARGET nav_msgs_generate_messages_py)
  add_dependencies(gta6_generate_messages_py nav_msgs_generate_messages_py)
endif()
