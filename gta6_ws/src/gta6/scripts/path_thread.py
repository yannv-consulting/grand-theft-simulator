import threading
import queue

class PathThread (threading.Thread):
    def __init__(self, args=()):
        threading.Thread.__init__(self)
        self.path = args[0]
        self.command_source = args[1]
        self.command_target = args[2]
        self.matrix = args[3]
        self.back_ground = args[4]
        self.screen = args[5]
        self.my_queue = args[6]

    def run(self):
        pathList = self.path.goto(self.command_source, self.command_target, self.matrix, self.back_ground, self.screen)
        self.my_queue.put(pathList)
