import sys
import math
import time

import map_segmentation

class AStar():
    def __init__(self, width, height, pixel_coef):
        self.map_width = width
        self.map_height = height
        self.pixel_coef = pixel_coef
        
        self.MAP_OBSTACLE_VALUE = 0

    def goto(self, command_source, command_target, matrix, back_ground, screen):
        source = {'x':command_source[0] // self.pixel_coef, 'y':command_source[1] // self.pixel_coef}
        target = {'x':command_target[0] // self.pixel_coef, 'y':command_target[1] // self.pixel_coef}

        # Dictionary that holds the previous node reference
        prev = {}
        # Dictionary that holds node score
        fscore = {}
        # Dictionary that holds node score
        gscore = {}
        # List that holds the nodes to process
        openList = []
        # List that holds the nodes already processed
        closedList = []

        INF=9999

        # Initialisation
        for i in range (len(matrix)):
            for j in range(len(matrix[0])):
                # All nodes receive a score of INF
                fscore[str(i) + '_' + str(j)] = INF
                gscore[str(i) + '_' + str(j)] = INF
                # All nodes are added to the list to process
                openList.append({'x':i, 'y':j})
        # Score of the start node is set to 0
        fscore[str(source['x']) + '_' + str(source['y'])] = 0
        gscore[str(source['x']) + '_' + str(source['y'])] = 0

        # While their is node to process or goal is reached (early exit)
        while len(openList) != 0:
            time.sleep(0.01)
            # Get the node with the lowest score
            u=self.minScore(fscore, openList)
            if str(u['x']) + '_' + str(u['y']) == str(target['x']) + '_' + str(target['y']):
                # end the path computation
                break
            # Remove the current node from the openList and add it to the path
            openList.remove(u)
            closedList.append(u)

            # Get the list of the neighbors of the current node
            currentNeighbors = self.getNeighbors(u, matrix)
            for v in currentNeighbors:
                if self.inU(v, openList):
                    continue

            # Check that the current node has not already be processed
            for v in currentNeighbors:
                if self.inU(v, closedList):
                    continue
                current_score = gscore[str(u['x']) + '_' + str(u['y'])] + 1

                if not self.inU(v, openList):
                    openList.append(v)
                elif current_score >= gscore[str(v['x']) + '_' + str(v['y'])]:
                    continue
                prev[str(v['x']) + '_' + str(v['y'])] = str(u['x']) + '_' + str(u['y'])
                gscore[str(v['x']) + '_' + str(v['y'])] = current_score
                fscore[str(v['x']) + '_' + str(v['y'])] = current_score + self.hn(matrix, v, target) + self.cost(matrix, v)
        pathList = self.sortDict(prev, command_target, back_ground, screen)
        return pathList

    def sortDict(self, prevList, command_target, back_ground, screen):
        newList = []

        x = command_target[0] // self.pixel_coef
        y = command_target[1] // self.pixel_coef
        try:
            prev = prevList[str(int(x)) + '_' + str(int(y))]
        except KeyError:
            pass

        newList.append([x * self.pixel_coef, y * self.pixel_coef])
        try:
            while prev != None:
                time.sleep(0.01)
                x = int(prev.split('_')[0])
                y = int(prev.split('_')[1])
                newList.append([x * self.pixel_coef, y * self.pixel_coef])
                prev = prevList[str(x) + '_' + str(y)]
        except KeyError:
                pass
        return newList

    def minScore(self, fscore,openList):
        """ Return the node that has the lowest score, information return like u={'x':5,'y':3}"""
        min = 9999
        min_coord = ''
        for n in openList:
            if(fscore[str(n['x']) + '_' + str(n['y'])] < min):
                min = fscore[str(n['x']) + '_' + str(n['y'])]
                min_coord = n
        return min_coord

    def getNeighbors(self, currentNode, matrix):
        """ Compute Neighbors of the current point, Return the list of the point neighbors in Cfree"""
        x_c = currentNode['x']
        y_c = currentNode['y']
        neighbors = []
        self.checkAndAdd(neighbors, x_c + 1, y_c, matrix)
        self.checkAndAdd(neighbors, x_c, y_c + 1, matrix)
        self.checkAndAdd(neighbors, x_c - 1, y_c, matrix)
        self.checkAndAdd(neighbors, x_c, y_c - 1, matrix)
        return neighbors

    def checkAndAdd(self, neighbors, x, y, matrix):
        """ Check that the candidate neighbor is valid == not an obstacle, in current bound, add the nieghbor node to the node list"""
        if (x > 0 and x < self.map_width and y > 0 and y < self.map_height):
             if (matrix[y][x] != self.MAP_OBSTACLE_VALUE):
                neighbors.append({'x': x, 'y': y})
        return neighbors

    def hn(self, matrix, source, destination):
        """Compute the distance between the given node and the target, the result is an estimation of the distance without taking into account obstacles """
        return math.sqrt(math.pow(source['x'] - destination['x'], 2) + math.pow(source['y'] - destination['y'], 2))

    def inU(self, v, openList):
        """ Check if the node is into the list, return boolean """
        return v in openList

    def cost(self, matrix, source):
        """ Return the cost of a pixel """
        x = source['x']
        y = source['y']
        gross_cost = matrix[y][x]
        cost = round((255 - gross_cost) * 5 / 25)
        return cost
