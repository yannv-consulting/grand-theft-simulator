#!/usr/bin/env python

import pygame

class Background(pygame.sprite.Sprite):
    def __init__(self, image_file, location, zoom_coef):
        # Call Sprite initializer
        pygame.sprite.Sprite.__init__(self)
        
        self.image = pygame.image.load(image_file)

        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = location
        self.image = pygame.transform.scale(self.image, (zoom_coef * self.rect.width, zoom_coef * self.rect.height))
        
    def restart_screen(self, screen):
        run = True
        # Restart loop
        while run:
            pygame.time.delay(100)
            
            # User input
            pressed = pygame.key.get_pressed()
            
            # Check events
            for event in pygame.event.get():
                if pressed[pygame.K_q]:
                    run = False
                    pygame.quit()
                if pressed[pygame.K_r]:
                    run = False
                    screen.fill((50, 50, 50))
            if pressed[pygame.K_r]:
                break

            # Restart message
            w,h = screen.get_size()
            self.message_display("YOU CRASHED. Press R to restart or Q to quit the simulation.", screen, w, h, (150, 50, 50))

    def busted_screen(self, screen):
        run = True
        # Restart loop
        while run:
            pygame.time.delay(100)
            
            # User input
            pressed = pygame.key.get_pressed()
            
            # Check events
            for event in pygame.event.get():
                if pressed[pygame.K_q]:
                    run = False
                    pygame.quit()
                if pressed[pygame.K_r]:
                    run = False
                    screen.fill((50, 50, 50))
            if pressed[pygame.K_r]:
                break

            # Busted message
            w,h = screen.get_size()
            self.message_display("BUSTED. Press R to restart or Q to quit the simulation.", screen, w, h, (50, 50, 150))
            
    def message_display(self, text, screen, w, h, color):
        large_text = pygame.font.Font('freesansbold.ttf', 30)
        text_surf, text_rect = self.text_objects(text, large_text)
        text_rect.center = ((w / 2), (h / 2))
        screen.fill(color)
        screen.blit(text_surf, text_rect)
        pygame.display.update()
    
    def text_objects(self,text, font):
        text_surface = font.render(text, True, (255,50,50))
        return text_surface, text_surface.get_rect()
